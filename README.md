## Plantsnear.me Frontend

Do the typical stuff before running the project for the first time:
`npm install`
then, run with 
`npm start`


## CI
building and testing occurs automatically. Deploying to production must be confirmed by Clicking the Start button
inside the Gitlab UI (CI/CD -> Pipelines).

## AWS Resources 
These links should all lead to the same page, if our CI works correctly ;D

S3 Bucket: http://plants-fe.s3-website-eu-west-1.amazonaws.com/
Cloudfront Distribution: https://d1s7ge4hkvu87j.cloudfront.net/
Domain: https://plantsnear.me


