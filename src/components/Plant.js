import React from 'react';

const Plant = (props) => {
  const { title, species, latin, description, author, id, authorid, time, imageurl, galleryurls } = props;
  
  return (
    <div>
      <h2>{title} - id: {id}</h2>
      <h3>
        <p>{species}</p>
        <p>{latin}</p>
      </h3>
      <p>{author} - {authorid}</p>
      <p>{description}</p>
      <img
        src={imageurl}
        alt={imageurl}
      />

      {galleryurls && galleryurls.map((url, i) => 
        <img src={url} key={i + url} alt={url}/>
      )}

      <p>{time}</p>
    </div>
  );
};

export default Plant;