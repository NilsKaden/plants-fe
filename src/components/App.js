import React from 'react';
import Plantprovider from "./PlantProvider"
import '../styles/App.css';

function App() {
  return (
    <div className="App">
      <Plantprovider/>
    </div>
  );
}

export default App;
