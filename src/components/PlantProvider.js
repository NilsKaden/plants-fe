import React, { useEffect, useState } from 'react';
import Plant from './Plant';

const Plantprovider = () => {
  const [plants, setPlants] = useState([]);
  const [id, setId] = useState(1);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    getPlants();
  }, [id]);

  const getPlants = async () => {
    const res = await fetch(`https://8lsqrg05ck.execute-api.eu-west-1.amazonaws.com/staging/plants`)
    if (res.status === 200) {
      setHasError(false)
      setPlants(await res.json());
    } else {
      setHasError(true)
    }
  }

  return (
    <div>
      <h1>Enter ID of plant</h1>
      <input type="number" min="1" max="10" onChange={e => setId(e.target.value)} value={id}/>

      {hasError &&
        <p>an error occured fetching item with id: {id}</p>
      }

      <hr/>


      {plants && plants.map(plant => 
        <Plant
          key={plant.id}
          title={plant.title}
          species={plant.species}
          description={plant.description}
          latin={plant.latin}
          author={plant.author}
          id={plant.id}
          authorid={plant.authorid}
          imageurl={plant.imageurl}
          galleryurls={plant.galleryurls}
          time={plant.time}
        />
      )}
      
    </div>
  );
};

export default Plantprovider;